import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {Page} from "../model/page";
import {User} from "../model/user";
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";


@Injectable({providedIn: 'root'})
export class UserService {

  private apiUrl = `${environment.apiServerUrl}/user`;

  constructor(private http: HttpClient) {
  }

  searchUsers(): Observable<Page<User>> {
    return this.http.get<Page<User>>(`${this.apiUrl}`);
  }
}
