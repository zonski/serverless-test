export interface Page<ItemType> {
  items: ItemType[];
  totalItems: number;
}
