import {Observable, of} from "rxjs";
import {catchError, map, startWith} from "rxjs/operators";

export interface Result<T> {
  loading: boolean;
  value: T;
  error: Error;
}

export type ObservableResult<T> = Observable<Result<T>>;

export function resultWithStatus<T>(obs: Observable<T>): Observable<Result<T>> {
  return obs.pipe(
    map(x => ({loading: false, value: x, error: null})),
    startWith({loading: true, value: null, error: null}),
    catchError((err: Error) => {
      return of({loading: false, value: null, error: err});
    })
  );
}
