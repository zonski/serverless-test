import {Component, OnInit, ViewChild} from '@angular/core';
import {UserService} from "../../../../services/user.service";
import {User} from "../../../../model/user";
import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {BehaviorSubject, Observable} from "rxjs";
import {tap} from "rxjs/operators";
import {MatPaginator} from "@angular/material/paginator";

@Component({
  selector: 'app-search-users',
  templateUrl: './search-users.component.html',
  styleUrls: ['./search-users.component.scss']
})
export class SearchUsersComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;

  dataSource: TableDataSource;
  displayedColumns = ['email', 'firstName', 'lastName'];

  constructor(private userService: UserService) {
  }

  ngOnInit(): void {
    this.dataSource = new TableDataSource(this.userService);
    this.dataSource.loadUsers();
  }

  ngAfterViewInit() {
    this.paginator.page
      .pipe(tap(() => this.loadPageOfUsers()))
      .subscribe();
  }

  onRowClicked(user: User) {
    console.log('Row clicked: ', user);
  }

  loadPageOfUsers() {
    console.log('Loading page of users');
    this.dataSource.loadUsers(
      '',
      'asc',
      this.paginator.pageIndex,
      this.paginator.pageSize);
  }

}

class TableDataSource implements DataSource<User> {

  private usersSubject = new BehaviorSubject<User[]>([]);

  private loadingSubject = new BehaviorSubject<boolean>(false);
  private errorSubject = new BehaviorSubject<boolean>(false);
  private currentPageSubject = new BehaviorSubject<any>(null);

  public loading$ = this.loadingSubject.asObservable();
  public error$ = this.errorSubject.asObservable();
  public currentPage$ = this.currentPageSubject.asObservable();

  constructor(private userService: UserService) {
  }

  connect(collectionViewer: CollectionViewer): Observable<User[]> {
    return this.usersSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.usersSubject.complete();
    this.loadingSubject.complete();
  }

  loadUsers(filter = '', sortDirection = 'asc', pageIndex = 0, pageSize = 3) {
    this.loadingSubject.next(true);
    this.usersSubject.next([]);
    this.errorSubject.next(null);
    this.userService.searchUsers().subscribe(result => {
      this.loadingSubject.next(false);
      this.usersSubject.next(result.items ? result.items : []);
      this.currentPageSubject.next(result);
    }, (response) => {
      this.loadingSubject.next(false);
      this.errorSubject.next(response.error);
    });
  }
}

