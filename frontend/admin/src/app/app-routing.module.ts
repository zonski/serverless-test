import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {NotFoundComponent} from "./components/util/not-found/not-found.component";
import {HomeComponent} from "./components/portal/home/home.component";
import {PortalComponent} from "./components/portal/portal.component";
import {SearchUsersComponent} from "./components/portal/user/search/search-users.component";


const routes: Routes = [
  {
    path: '',
    component: PortalComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: HomeComponent
      },
      {
        path: 'user',
        children: [
          {
            path: '',
            pathMatch: 'full',
            component: SearchUsersComponent
          }
        ]
      }
    ]
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
