export interface TodoItem {
    title: string;
    description: string;
    createdAt?: string;
    updatedAt?: string;
}
