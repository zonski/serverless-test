import {APIGatewayEvent} from "aws-lambda";
import * as auth from "./sercurity.service";
import * as api from "../api/api.util";

export const getSettings = async (event: APIGatewayEvent): Promise<any> => {
    const settings = auth.getUserPoolSettings();
    return api.success(settings);
};

export const registerUser = async (event: APIGatewayEvent): Promise<any> => {
    let request = api.parseBody(event);
    let result = await auth.registerUser(request);
    return api.success(result);
};

