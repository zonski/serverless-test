import {APIGatewayEvent} from "aws-lambda";
import {knex} from "./knex";
import {TodoItem} from "./todo-item";
import * as api from "./api.util";

export const findAllTodoItems = async (event: APIGatewayEvent): Promise<any> => {
    let response = await knex<TodoItem>('todo_item');
	return api.success(response);
};

export const findTodoItem = async (event: APIGatewayEvent): Promise<any> => {
	let id = api.param(event, 'id');
	let response = await knex<TodoItem>('todo_item')
		.where({ id: id })
		.first();
	return api.success(response);
};

export const addTodoItem = async (event: APIGatewayEvent): Promise<any> => {
	let request = api.parseBody(event);
	let ids = await knex<TodoItem>('todo_item')
		.insert(request)
		.returning('id');
	return api.success({ id: ids[0]});
};
