import * as Knex from 'knex'
import * as knexStringcase from 'knex-stringcase'

export const knex: Knex = Knex(knexStringcase({
    client: 'pg',
    connection: {
        host: '127.0.0.1',
        user: 'serverless-test',
        password: 'serverless-test',
        database: 'serverless-test'
    },
    pool: { min: 2, max: 10 }
}));
