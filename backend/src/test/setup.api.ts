import {APIGatewayEvent} from "aws-lambda";
import {knex} from "./knex";
import {TodoItem} from "./todo-item";
import * as api from "./api.util";

export const setupDatabase = async (event: APIGatewayEvent): Promise<any> => {

    await knex.schema.createTable('todo_item', function(table) {
		table.increments('id');
		table.string('title').notNullable();
		table.string('description').notNullable();
		table.timestamp('created_at').defaultTo(knex.fn.now());
		table.timestamp('updated_at').defaultTo(knex.fn.now());
	});

    let item: TodoItem = {
		title: 'Test Todo',
		description: 'This is a test todo'
	};
    await knex<TodoItem>('todo_item').insert(item);

	return api.success({ message: 'Setup complete'});
};
