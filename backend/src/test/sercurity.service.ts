import {CognitoUserPool, ICognitoUserPoolData, ISignUpResult} from 'amazon-cognito-identity-js';

const {USER_POOL_ID, USER_POOL_CLIENT_ID} = process.env;

const PC: ICognitoUserPoolData = {
    UserPoolId: USER_POOL_ID ? USER_POOL_ID : '',
    ClientId: USER_POOL_CLIENT_ID ? USER_POOL_CLIENT_ID : ''
};
const userPool = new CognitoUserPool(PC);

export const getUserPoolSettings = () => {
    return {
        userPoolId: USER_POOL_ID,
        userPoolClientId: USER_POOL_CLIENT_ID
    };
};

export const registerUser = async (request: { email: string, password: string }): Promise<ISignUpResult> => {
    let attributeList = [];
    return new Promise<ISignUpResult>((resolve, reject) => {
        userPool.signUp(request.email, request.password, attributeList, [], (err, response) => {
            if (err) {
                reject(err);
            } else {
                resolve(response);
            }
        });
    });
};
