import {APIGatewayEvent} from "aws-lambda";
import * as api from "../api.util";

const snooze = ms => new Promise(resolve => setTimeout(resolve, ms));

export const searchUsers = async (event: APIGatewayEvent): Promise<any> => {

    await snooze(1000);

    let results = {
        totalItems: 2,
        items: [
            {
                id: 0,
                email: 'zonski@gmail.com',
                firstName: 'Dan',
                lastName: 'Zwolenski'
            },
            {
                id: 1,
                email: 'dan@digitalpurpose.com.au',
                firstName: 'Test',
                lastName: 'User'
            }
        ]
    };
    return api.success(results);
};
