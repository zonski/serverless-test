import {APIGatewayEvent} from "aws-lambda";

export const param = (event: APIGatewayEvent, param: string) => {
    return event.pathParameters ? event.pathParameters[param] : null
};

export const parseBody = (event: APIGatewayEvent, errorIfNone = true) => {
    if (!errorIfNone && !event.body) {
        return null;
    }

    if (event.body) {
        return JSON.parse(event.body);
    } else {
        throw 'No details included in POST';
    }
};

export const success = (result) => {
    return {
        statusCode: 200,
        body: JSON.stringify(result)
    }
};

export const error = (error) => {
    return {
        statusCode: 500,
        body: JSON.stringify(error)
    }
};
